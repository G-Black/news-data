
import { useState } from 'react';
import NewsList from '../components/news/NewsList';
import Navbar from '../components/Navbar';

const categories = ['', 'business', 'entertainment', 'environment', 'food', 'health', 'politics', 'science', 'sports', 'technology', 'top', 'world'];

export default function News() {
    const [category, setCategory] = useState(null);

    function handleFilterByCategories(e) {
        setCategory(e.target.value);
    }
    return (
        <>
            <Navbar />
            <div className='container mt-5 pt-md-5 pt-4'>
                <section className='row mb-4'>
                    <div className='col-sm-12 col-md-6 col-lg-6 col-xl-4 offset-md-3 offset-lg-3 offset-xl-4 text-center'>
                        <label className='mb-2'>Filter by category:</label>
                        <select className='form-select' onChange={handleFilterByCategories}>
                            {categories.map((item, index) => (
                                <option key={index} value={item}>{item}</option>
                            ))}
                        </select>
                    </div>
                </section>
                <NewsList category={category} />
            </div>
        </>
    )
}