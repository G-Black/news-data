import { useEffect, useRef, useState } from 'react';
import Spinner from '../../widgets/Spinner';
import NewsCard from './NewsCard';

export default function NewsList({category}) {
    const [page, setPage] = useState(1)
    const [news, setNews] = useState([]);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);
    const loader = useRef(null);

    const handleObserver = (entities) => {
        const target = entities[0]
        if (target.isIntersecting) {
          setPage(_page => _page + 1)
        }
    }

    useEffect(() => {
        const options = {
          root: null,
          rootMargin: '20px',
          threshold: 1.0,
        }
        const observer = new IntersectionObserver(handleObserver, options)
        if (loader.current) {
          observer.observe(loader.current)
        }
    }, [])

    useEffect(function(){
        async function fetchNews(){
            setLoading(true);
            try {
                const response = await fetch(`${process.env.REACT_APP_API_URL}news?page=${page}&apiKey=${process.env.REACT_APP_NEWS_API_KEY}${category ? '&category='+category : ''}&language=en`);
                if(!response.ok) setError('Something went wrong while fetching..');
                const data = await response.json();
                if(!category){
                    const updatedNews = news?.concat(data?.results)
                    setNews(updatedNews);
                } else {
                    setNews(data.results);
                }
                setLoading(false);
            } catch (error) {
                console.log(error)
                setLoading(false)
                setError(error.message);
            }
        }
        fetchNews();
        // eslint-disable-next-line
    }, [page, category]);

    return (
        <div className='row'>
            {loading && renderLoadingResponse()
            }
            {error && renderErrorResponse(error)}
            {renderNews(news)}
             <div ref={loader} className='text-center'>
                { news?.length > 0 && <Spinner /> }
            </div>
        </div>
    )
}

function renderNews(news){
    return news?.map((item, index) => (
        <NewsCard news={item} key={index} />
    ))
}

function renderLoadingResponse(){
    return <div className='text-center'>
        <div className="spinner-border primary_color" role="status">
            <span className="visually-hidden">Loading...</span>
        </div>
    </div>
}

function renderErrorResponse(error){
    return <div className='alert alert-danger'>{error.message ?? "Error while fetching news..."}</div>;
}