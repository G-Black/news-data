import dayjs from 'dayjs';
import fallbackImage from "../../assets/images/fallback-image.jpeg";

export default function NewsCard({news}){
    return (
        <section className="col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-4">
            <div className="card news_card h-100">
                <img src={news?.image_url ?? fallbackImage} className="card-img-top news_card__image" alt={news?.title} />
                <div className="card-body position-relative">
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="flex-fill">
                            <div className="card-text text-end text-muted small">{dayjs(news?.pubDate).format('ddd D MMM, YYYY')}</div>
                            <a href={news?.link} target="_blank" rel="noreferrer" className='text-decoration-none'>
                                <h6 className="card-title primary_color fw-bold">{news?.title},</h6>
                            </a>
                        </div>
                    </div>
                    <p >{renderNewsContent(news?.content)}</p>
                    <div className="card-text small mb-2">
                        {news?.creator?.length > 0 && <div className='fw-bold'>Author(s):</div>}
                        {news?.creator?.map((item, index) => <span className='text-muted' key={index}>{item}</span>)}
                    </div>
                    <div className="small">
                        {news?.keywords?.length > 0 && <div className='fw-bold mb-1'>Keywords:</div>}
                        {news?.keywords?.map((item, index) => <div key={index} className="bg-secondary d-inline-block text-white rounded py-1 me-1 mb-1 px-2">{item}</div>)}
                    </div>
                </div>
            </div>
        </section>
    )
}

function renderNewsContent(content){
    // console.log(content.length);
    if(content)
    return content.length > 200 ? content.substring(0, 200) + "..." : content;
    // return content;
}