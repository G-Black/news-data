
export default function Navbar() {
    return (
        <nav class="navbar navbar-expand-lg text-white bg-dark fixed-top">
            <div class="container-fluid justify-content-center">
                <h1>World News</h1>
            </div>
        </nav>
    )
}