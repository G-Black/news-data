export default function PageTitle({title}){
    return (
        <h1 className="mb-4 text-center">{title}</h1>
    )
}